import { Component } from '@angular/core';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { NativeGeocoder, NativeGeocoderOptions } from '@ionic-native/native-geocoder/ngx';


@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  lat:number;
  lon:number;
  address:string;
  gpsLocation:string;
  watch:any; 
  constructor(private geolocation: Geolocation,private nativeGeocoder: NativeGeocoder) {
    this.lat = 0; 
    this.lon = 0; 
    this.address = "";
    this.gpsLocation = ""; 
  }
  forwardGeocode()
  {
    let options: NativeGeocoderOptions = {
      useLocale: true,
      maxResults: 5
    };
    this.nativeGeocoder.forwardGeocode('Abu Dhabi University', options)
      .then((coordinates) => 
          {
            this.gpsLocation = 
                                'The coordinates are latitude=' 
                                + coordinates[0].latitude 
                                + ' and longitude=' 
                                + coordinates[0].longitude;
            
          })
      .catch((error: any) => console.log(error));
  }
  findLocationAndReverseGeocode()
  {
    this.geolocation.getCurrentPosition().then(
      (data)=>{
        this.lat = data.coords.latitude;
        this.lon = data.coords.longitude;
        let options: NativeGeocoderOptions = {
          useLocale: true,
          maxResults: 5
        };
        this.nativeGeocoder.reverseGeocode(this.lat, this.lon, options)
          .then((result) => {
            
              this.address = "You are in " +  result[0].administrativeArea +
              " of the " + result[0].countryName; 
          
            }
          )
          .catch((error: any) => console.log(error));

      }
    );
  }

  reverseGeocode()
  {
    let options: NativeGeocoderOptions = {
      useLocale: true,
      maxResults: 5
    };
    this.nativeGeocoder.reverseGeocode(this.lat, this.lon, options)
      .then((result) => {
        
          this.address = "You are in " +  result[0].administrativeArea +
          " of the " + result[0].countryName; 
      
        }
      )
      .catch((error: any) => console.log(error));
  }
  findLocationOnce(){
    this.geolocation.getCurrentPosition().then(
      (data)=>{
        this.lat = data.coords.latitude
        this.lon = data.coords.longitude
      }
    );
  }
  findLocationRepeatedly()
  {
    this.watch = this.geolocation.watchPosition();
    this.watch.subscribe(
      (data)=>{
        this.lat = data.coords.latitude
        this.lon = data.coords.longitude
      }
    );
  }
  stopFindingLocation()
  {
    this.watch.stop();
    this.lat = 0; 
    this.lon = 0;  
  }

}
